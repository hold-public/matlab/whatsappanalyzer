function analyseWhatsappChat
    % analyses a whatsapp chat and makes numerical psychologial analysis
    % author: Patrick Haldi

    clc
    
    addpath 'auxiliary functions'
    %% definitions
    FIXED_LANGUAGE = false;
    if FIXED_LANGUAGE
        languageNr = 1;
        warning('Language set to Swiss German')
    else
        languageNr = menu('choose your language', 'Swiss German', 'High German');
    end
    if languageNr == 1
        % swiss german
        meWords =   {'mi',  'ig', 'mir', 'mis', 'mini'};
        youWords =  {'di',  'du', 'dir', 'dis', 'dini'};
        niceWords = {'danke', 'merci', 'bitte', 'n�tt', 'sch�n', 'interessant', 'grossartig', 'geil',...
            'kuul', 'gratulir�', 'gratulier�', 'super', 'toll', 'fantastisch', 'geniau', 'chea',...
            'yeah'};
        strongWords = {'fuck', 'scheiss', 'wixer', 'wichser', 'arsch', 'depp', 'doof', 'gigu'};
    elseif languageNr == 2
        % high german
        meWords =   {'mich', 'ich', 'mir', 'mein', 'meine', 'meins'};
        youWords =  {'dich', 'du',  'dir', 'dein', 'deine', 'deins'};
        niceWords = {'danke', 'bitte', 'nett', 'sch�n', 'interessant', 'grossartig'};
        strongWords = {'fuck', 'scheiss', 'wixer', 'wichser', 'arsch', 'depp', 'doof'};
    else
        error('this language does not yet exist!')
    end
    %% loading List of Measurements (LoM)
    inputFolder = 'chat logs';
    [inputFileName, inputPath, ~] = uigetfile([inputFolder '\*.txt']);
    fileString = fileread([inputPath '\' inputFileName]);
    fileString = replaceUmlauts(fileString);
    
    %% split text to messages
    timestampExpEng =    '\d\d:\d\d, \d\d? \D\D\D \d* ?- ';
    timestampExpIphone = '\d\d.\d\d.\d\d \d\d:\d\d:\d\d: ';
    timestampExpGer =    '\d?\d. \D\D\D.? \d* ?\d\d\:\d\d -';
    timestampExpX =      '\d\d/\d\d/\d\d\d\d, \d\d:\d\d - ';
    timestampExp = ['((' timestampExpEng ')|(' timestampExpGer ')|(' timestampExpIphone ')|(' timestampExpX '))'];
    [timestampStartIndexes, timeStampEndIndexes] = regexp(fileString, timestampExp);

    messageStartIndexes = timeStampEndIndexes + 1;
    messageStopIndexes = [timestampStartIndexes(2:end) - 1, length(fileString)];

    nrOfMessages = length(messageStartIndexes);

    names = cell(0,0);
    wordCounts = struct;
    if(nrOfMessages < 1)
        error('no messages found!')
    end
    for messageIndex = 1:nrOfMessages
        fullMessageText = fileString(messageStartIndexes(messageIndex):messageStopIndexes(messageIndex));
        if isWhatsappMessage(fullMessageText)
            continue
        end

        colonIndex = strfind(fullMessageText, ':');
        endOfNameIndex = colonIndex(1) - 1;
        senderName = fullMessageText(1:endOfNameIndex);
        nameFound = strcmp(names, senderName);
        if any(nameFound)
            sender = find(nameFound);
        else
            names = [names senderName]; %#ok<AGROW>
            sender = length(names);
            wordCounts.nrOfWords(sender) = 0;
            wordCounts.nrOfMes(sender) = 0;
            wordCounts.nrOfYous(sender) = 0;
            wordCounts.nrOfNices(sender) = 0;
            wordCounts.nrOfStrongs(sender) = 0;
            wordCounts.wordList{sender} = cell(0, 2);
        end

        messageTextWithCommas = strtrim(lower(fullMessageText(endOfNameIndex + 2:end)));
        messageTextWithReturns = strrep(messageTextWithCommas, ',', '');
        messageText = strrep(messageTextWithReturns, sprintf('\r\n'), ' ');
        words = strsplit(messageText, ' ', 'CollapseDelimiters', 1);
        nrOfMessageWords = length(words);

        wordCounts.wordList{sender} = addToWordList(wordCounts.wordList{sender}, words);
        wordCounts.nrOfWords(sender) = wordCounts.nrOfWords(sender) + nrOfMessageWords;
        wordCounts.nrOfMes(sender) = wordCounts.nrOfMes(sender) + countWords(words, meWords);
        wordCounts.nrOfYous(sender) = wordCounts.nrOfYous(sender) + countWords(words, youWords);
        wordCounts.nrOfNices(sender) = wordCounts.nrOfNices(sender) + countWords(words, niceWords);
        wordCounts.nrOfStrongs(sender) = wordCounts.nrOfStrongs(sender) + countWords(words, strongWords);
    end

    nrOfNames = length(names);
    for nameIndex = 1:nrOfNames
        wordCounts.meIndex(nameIndex) = 100 * wordCounts.nrOfMes(nameIndex) / wordCounts.nrOfWords(nameIndex);
        wordCounts.youIndex(nameIndex) = 100 * wordCounts.nrOfYous(nameIndex) / wordCounts.nrOfWords(nameIndex);
        wordCounts.niceIndex(nameIndex) = 100 * wordCounts.nrOfNices(nameIndex) / wordCounts.nrOfWords(nameIndex);
        wordCounts.strongIndex(nameIndex) = 100 * wordCounts.nrOfStrongs(nameIndex) / wordCounts.nrOfWords(nameIndex);
        outputResults(names{nameIndex}, nameIndex, wordCounts);
        disp(' ')
    end

    summaryOut(names, wordCounts);
end
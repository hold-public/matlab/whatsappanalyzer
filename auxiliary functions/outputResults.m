function outputResults(name, nameIndex, wordCounts)
    disp(['nr of ' name '''s words: ' num2str(wordCounts.nrOfWords(nameIndex))])
    disp(['nr of ' name '''s me-words: ' num2str(wordCounts.nrOfMes(nameIndex))])
    disp(['nr of ' name '''s you-words: ' num2str(wordCounts.nrOfYous(nameIndex))])
    disp(['nr of ' name '''s nice words: ' num2str(wordCounts.nrOfNices(nameIndex))])
    disp(['nr of ' name '''s strong words: ' num2str(wordCounts.nrOfStrongs(nameIndex))])
    disp(['nr of ' name '''s me-words per 100 words: ' num2str(wordCounts.meIndex(nameIndex), 2)])
    disp(['nr of ' name '''s you-words per 100 words: ' num2str(wordCounts.youIndex(nameIndex), 2)])
    disp(['nr of ' name '''s nice words per 100 words: ' num2str(wordCounts.niceIndex(nameIndex), 2)])
    disp(['nr of ' name '''s strong words per 100 words: ' num2str(wordCounts.strongIndex(nameIndex), 2)])
    
    wordList = wordCounts.wordList{nameIndex};
    isLongWord = cellfun(@length, wordList(:, 1)) > 5;
    longWordList = wordList(isLongWord,:);
    sortedWordList = sortrows(longWordList, -2);
    nrOfUsedLongWords = size(sortedWordList, 1);
    nrOfMostUsedWords = min(10, nrOfUsedLongWords);
    
    disp([name '''s most used words: '])
    for mostUsedWordIndex = 1:nrOfMostUsedWords
        thisWord = sortedWordList{mostUsedWordIndex, 1};
        thisCount = sortedWordList{mostUsedWordIndex, 2};
        disp(['''' thisWord ''': ' num2str(thisCount) 'x'])
    end
end
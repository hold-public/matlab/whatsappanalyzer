function wordList = addToWordList(wordList, newWords)
    nrOfWords = length(newWords);
    for wordIndex = 1:nrOfWords
        existingWords = wordList(:, 1);
        thisWord = newWords{wordIndex};
        sameWord = strcmp(existingWords, thisWord);
        if any(sameWord)
            if nnz(sameWord) > 1
                error('several same entries in word list!')
            end
            sameWordIndex = find(sameWord, 1, 'first');
            wordList{sameWordIndex, 2} = wordList{sameWordIndex, 2} + 1;
        else
            wordList = [wordList; {thisWord, 1}]; %#ok<AGROW>
        end
    end
end
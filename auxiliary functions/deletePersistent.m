function deletePersistent(fullFilePath)
    s = warning('error', 'MATLAB:DELETE:Permission');
    deletingSuceeded = false;
    while(~deletingSuceeded)
        try
            delete(fullFilePath)
            deletingSuceeded = true;
        catch err
            disp(['Cannot delete file "' fullFilePath '". Maybe it is locked. Please close file and try again.'])
            inputText = input('try again? y/n ', 's');
            if(~strcmp(inputText, 'y'))
                rethrow(err);
            end
        end
    end
    warning(s);
end
function text = replaceUmlauts(text)
    text = strrep(text, '�', '�');
    text = strrep(text, 'ä', '�');
    
    text = strrep(text, '�', '�');
    text = strrep(text, 'ü', '�');
    
    text = strrep(text, '�', '�');
    text = strrep(text, 'ö', '�');
end
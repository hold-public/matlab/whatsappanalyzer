function count = countWords(words, queryWords)
    count = 0;
    for wordIndex = 1:length(queryWords)
        currentQueryWord = strtrim(queryWords{wordIndex});
        queryWordIndexes = strcmp(words, currentQueryWord);
        count = count + nnz(queryWordIndexes);
    end
end
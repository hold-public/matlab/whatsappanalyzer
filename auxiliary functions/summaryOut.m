function summaryOut(names, wordCounts)
    [~, maxWordIndex] = max(wordCounts.nrOfWords);
    disp(['most chatty person: ' names{maxWordIndex}])
    
    [~, maxMeIndex] = max(wordCounts.meIndex);
    disp(['most self-orientated person: ' names{maxMeIndex}])
    
    [~, maxYouIndex] = max(wordCounts.youIndex);
    disp(['most other-orientated person: ' names{maxYouIndex}])
    
    [~, maxNiceIndex] = max(wordCounts.niceIndex);
    disp(['most positive and nice person: ' names{maxNiceIndex}])

    [~, maxStrongIndex] = max(wordCounts.strongIndex);
    disp(['most cursing person: ' names{maxStrongIndex}])
end
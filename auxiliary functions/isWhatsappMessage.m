function isWhatsappMessage = isWhatsappMessage(text)
    systemTexts = {' created group ', ' added you', ' was added',...
        'You changed this group''s icon', '<Media omitted>', 'changed the subject to', ...
        'You''re now an admin', ' hat den Betreff zu ', ' wurde hinzugef�gt', ' hat das Gruppenbild ge�ndert',...
        ' hat die Gruppe verlassen', ' wurde entfernt', '<Medien weggelassen>'};
    isWhatsappMessage = false;
    for systemTextIndex = 1:length(systemTexts)
        if ~isempty(strfind(text, systemTexts{systemTextIndex}))
            isWhatsappMessage = true;
        end
    end
end

